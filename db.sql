create database x_o;
use x_o;

create table plays
(
    id            int auto_increment primary key,
    first_player  nvarchar(100),
    second_player nvarchar(100),
    result        int
);

create table states
(
    id      int auto_increment primary key,
    play_id int not null references plays (id),
    cell0   int not null,
    cell1   int not null,
    cell2   int not null,
    cell3   int not null,
    cell4   int not null,
    cell5   int not null,
    cell6   int not null,
    cell7   int not null,
    cell8   int not null
);