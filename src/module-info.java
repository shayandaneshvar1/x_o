module Tic.Tac.Toe {
    requires javafx.swing;
    requires javafx.web;
    requires javafx.fxml;
    requires javafx.swt;
    requires javafx.base;
    requires javafx.media;
    requires javafx.controls;
    requires javafx.graphics;

    requires mysql.connector.java;
    requires java.sql;

    requires com.jfoenix;

    opens main.java.controller;
    opens main.java.view;
    opens main.java.repository;
    opens main.java.model;
}