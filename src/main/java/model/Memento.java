package main.java.model;

public final class Memento {
    private Board board;

    public Memento(Board board) {
        this.board = new Board();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.board.putCell(board.getCell(i, j), i, j);
            }
        }
    }
    public Board getBoard() {
        return board;
    }
}
