package main.java.model;

import main.java.controller.Controller;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private Integer id;
    private final Controller controller;
    private Board board;
    private String firstPlayer;
    private String secondPlayer;
    private int turn;
    private String state;
    private List<Memento> saves;

    public Game(Board board, String firstPlayer, String secondPlayer,
                Controller controller) {
        this.board = board;
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        turn = 0;
        this.controller = controller;
        saves = new ArrayList<>();
    }

    public Game(Board board, String firstPlayer, String secondPlayer) {
        this.board = board;
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        saves = new ArrayList<>();
        controller = null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Game(Controller controller) {
        this(new Board(), controller);
    }

    public Game(Board board, Controller controller) {
        this(board, "First Player", "Second Player", controller);
    }


    public String getFirstPlayer() {
        return firstPlayer;
    }

    public String getSecondPlayer() {
        return secondPlayer;
    }

    public int getTurn() {
        return turn;
    }

    /**
     * checks whether the game
     *
     * @return bool
     */
    private boolean isBoardFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board.getCell(i, j) == ' ') {
                    return false;
                }
            }
        }
        return true;
    }

    private void goToNextRound() {
        turn++;
    }

    public void handlePlacement(int x, int y) {
        if (board.getCell(x, y) == ' ') {
            saveState();
            board.putCell(turn % 2 == 0 ? 'x' : 'o', x, y);
            handleGameOver();
            goToNextRound();
        } else {
            assert false;
        }
    }

    public List<Memento> getSaves() {
        return saves;
    }

    private void handleGameOver() {
        if (hasPlayer1Won()) {
            controller.handleTheEnd(firstPlayer + " Has Won!", 1);
        } else if (hasPlayer2Won()) {
            controller.handleTheEnd(secondPlayer + " Has Won!", 2);
        } else if (isBoardFull()) {
            controller.handleTheEnd("Draw!", 0);
        }
    }

    private boolean hasPlayer2Won() {
        return hasWon('o');
    }

    private boolean hasPlayer1Won() {
        return hasWon('x');
    }

    private boolean hasWon(char character) {
        for (int i = 0; i < 3; i++) {
            if (board.getCell(i, 0) == character && board.getCell(i, 1) == character
                    && board.getCell(i, 2) == character) {
                return true;
            } else if (board.getCell(0, i) == character && board.getCell(1, i) == character
                    && board.getCell(2, i) == character) {
                return true;
            }
        }
        if (board.getCell(0, 0) == character && board.getCell(1, 1) == character
                && board.getCell(2, 2) == character) {
            return true;
        } else {
            if (board.getCell(2, 0) == character && board.getCell(1, 1) == character
                    && board.getCell(0, 2) == character) {
                return true;
            }
        }
        return false;
    }

    private void saveState() {
        saves.add(new Memento(this.board));
    }
}
