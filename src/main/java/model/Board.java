package main.java.model;

import java.util.ArrayList;
import java.util.List;

public class Board {
    private char[][] cells;

    public Board(char[][] cells) {
        this.cells = cells;
    }

    public Board() {
        cells = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                cells[i][j] = ' ';
            }
        }
    }

    public void putCell(char character, int x, int y) {
        cells[y][x] = character;
    }

    public char getCell(int x, int y) {
        return cells[y][x];
    }

}
