package main.java.repository;

import main.java.model.Game;
import main.java.model.Memento;

import java.sql.*;

public final class Repository {
    private static final Repository INSTANCE = new Repository();
    private final int PORT = 3306;
    private final String PASSWORD = "123456";
    private final String USER = "root";

    private PreparedStatement insertPlay;
    private PreparedStatement selectPlays;
    private PreparedStatement selectByID;

    private PreparedStatement insertState;
    private PreparedStatement selectStates;

    private void prepareStatements(Connection connection) throws SQLException {
        selectPlays = connection.prepareStatement("SELECT * FROM x_o.plays");
        selectByID = connection.prepareStatement("SELECT * from  x_o.plays " +
                "where id = ?");
        insertPlay = connection.prepareStatement("INSERT INTO " +
                "x_o.plays VALUES (null ,?,?,?)", Statement.RETURN_GENERATED_KEYS);
        insertState = connection.prepareCall("INSERT INTO " +
                "x_o.states VALUES (null ,?,?,?,?,?,?,?,?,?,?)");
        selectStates = connection.prepareStatement("SELECT * from  x_o.states" +
                " where play_id = ?");
    }

    public int insert(Game game, int result) {
        try {
            insertPlay.setString(1, game.getFirstPlayer());
            insertPlay.setString(2, game.getSecondPlayer());
            insertPlay.setInt(3, result);
            int affectedRows = insertPlay.executeUpdate();
            assert affectedRows != 0;
            ResultSet generatedKeys = insertPlay.getGeneratedKeys();
            if (generatedKeys.next()) {
                game.setId(generatedKeys.getInt(1));
            } else {
                throw new SQLException("Creating user failed, no ID obtained.");
            }
            assert game.getId() != null;
            for (int i = 0; i < game.getSaves().size(); i++) {
                insert(game.getSaves().get(i), game.getId());
            }
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        assert false;
        return 1;
    }

    private int insert(Memento memento, int gameID) {
        try {
            insertState.setInt(1, gameID);
            insertState.setInt(2, memento.getBoard().getCell(0, 0));
            insertState.setInt(3, memento.getBoard().getCell(1, 0));
            insertState.setInt(4, memento.getBoard().getCell(2, 0));
            insertState.setInt(5, memento.getBoard().getCell(0, 1));
            insertState.setInt(6, memento.getBoard().getCell(1, 1));
            insertState.setInt(7, memento.getBoard().getCell(2, 1));
            insertState.setInt(8, memento.getBoard().getCell(0, 2));
            insertState.setInt(9, memento.getBoard().getCell(1, 2));
            insertState.setInt(9, memento.getBoard().getCell(2, 2));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static Repository getINSTANCE() {
        return INSTANCE;
    }
//    public List<Game> selectAll() {
//
//    }

//    public List<Game> selectByID(int gameID) {
//
//    }

    private Repository() {
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:" + PORT + "/x_o",
                    USER, PASSWORD);
            prepareStatements(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


//    public List<Game> fetchData(PreparedStatement statement) {
//        final List<Game> games = new ArrayList<>();
//        try {
//            ResultSet resultSet = statement.executeQuery();
//            while (resultSet.next()) {
//                Boa
//                Game game = new Game(statement.get);
//                games.add(game);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return games;
//    }
}
