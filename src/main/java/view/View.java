package main.java.view;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.java.controller.Controller;

import java.io.IOException;

public class View extends Application {
    private Controller controller;
    private Stage primaryStage;
    private String addressO = "file:///C:\\Users\\TOP\\Desktop\\AP " +
            "Assignments\\x_o\\src\\main\\resources\\ooo.jpg";
    private String addressX = "file:///C:\\Users\\TOP\\Desktop\\AP " +
            "Assignments\\x_o\\src\\main\\resources\\x.jpg";
    private static int changed = -1;

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/java" +
                "/view/game.fxml"));
        AnchorPane root = loader.load();
        Scene scene = new Scene(root);
        stage.setTitle("X O");
        stage.setScene(scene);
        stage.show();
        primaryStage = stage;
    }

    public View(Controller controller) {
        this.controller = controller;
    }

    public View() {
    }

    public static void setChanged(int changed) {
        View.changed = changed;
    }

    public void run() {
        GameController.setPictureAddress(addressX);
        new Thread(() -> {
            while (true) {
                if (changed >= 0) {
                    if (controller.handleChange(changed)) {
                        changed = -1;
                        if (controller.getGame().getTurn() % 2 == 0) {
                            GameController.setPictureAddress(addressX);
                        } else {
                            GameController.setPictureAddress(addressO);
                        }
                    }
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        launch();
    }

    public void gameOver(String statement) {
        Platform.runLater(() -> {
            try {
                Stage stage = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/java" +
                        "/view/gameOver.fxml"));
                AnchorPane pane = loader.load();
                Scene scene = new Scene(pane);
                stage.setTitle("X O");
                stage.setScene(scene);
                stage.initOwner(primaryStage);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
                stage.setOnCloseRequest(x -> {
                    System.exit(0);
                });
                Text text = new Text(statement);
                AnchorPane.setRightAnchor(text, 70d);
                AnchorPane.setLeftAnchor(text, 70d);
                AnchorPane.setTopAnchor(text, 70d);
                AnchorPane.setBottomAnchor(text, 70d);
                text.setStyle("-fx-font-size: 16px");
                pane.getChildren().add(text);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }
}
