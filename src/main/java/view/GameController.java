package main.java.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class GameController implements Initializable {
    private static String pictureAddress = null;
    private static boolean singleplayer = false;
    @FXML
    private ImageView cell0;

    @FXML
    private ImageView cell1;

    @FXML
    private ImageView cell2;

    @FXML
    private ImageView cell3;

    @FXML
    private ImageView cell4;

    @FXML
    private ImageView cell5;

    @FXML
    private ImageView cell6;

    @FXML
    private ImageView cell7;

    @FXML
    private ImageView cell8;

    @FXML
    void handleCell0(MouseEvent event) {
        cellHandler(cell0, 0);
    }

    private boolean cellHandler(ImageView cell, int id) {
        if (cell.getImage() == null) {
            cell.setImage(new Image(pictureAddress));
            View.setChanged(id);
            if (singleplayer) {
                playRandom();
            }
            return true;
        }
        return false;
    }

    private void playRandom() {
        if (cell0.getImage() == null) {
            cell0.setImage(new Image(pictureAddress));
            View.setChanged(0);
        } else if (cell1.getImage() == null) {
            cell1.setImage(new Image(pictureAddress));
            View.setChanged(1);
        } else if (cell2.getImage() == null) {
            cell2.setImage(new Image(pictureAddress));
            View.setChanged(2);
        } else if (cell3.getImage() == null) {
            cell3.setImage(new Image(pictureAddress));
            View.setChanged(3);
        } else if (cell4.getImage() == null) {
            cell4.setImage(new Image(pictureAddress));
            View.setChanged(4);
        } else if (cell5.getImage() == null) {
            cell5.setImage(new Image(pictureAddress));
            View.setChanged(5);
        } else if (cell5.getImage() == null) {
            cell5.setImage(new Image(pictureAddress));
            View.setChanged(5);
        } else if (cell6.getImage() == null) {
            cell6.setImage(new Image(pictureAddress));
            View.setChanged(6);
        } else if (cell7.getImage() == null) {
            cell7.setImage(new Image(pictureAddress));
            View.setChanged(7);
        } else if (cell8.getImage() == null) {
            cell8.setImage(new Image(pictureAddress));
            View.setChanged(8);
        }
    }

    public static void setSingleplayer(boolean singleplayer) {
        GameController.singleplayer = singleplayer;
    }

    @FXML
    void handleCell1(MouseEvent event) {
        cellHandler(cell1, 1);
    }

    @FXML
    void handleCell2(MouseEvent event) {
        cellHandler(cell2, 2);
    }

    @FXML
    void handleCell3(MouseEvent event) {
        cellHandler(cell3, 3);
    }

    @FXML
    void handleCell4(MouseEvent event) {
        cellHandler(cell4, 4);
    }

    @FXML
    void handleCell5(MouseEvent event) {
        cellHandler(cell5, 5);
    }

    @FXML
    void handleCell6(MouseEvent event) {
        cellHandler(cell6, 6);
    }

    @FXML
    void handleCell7(MouseEvent event) {
        cellHandler(cell7, 7);
    }

    @FXML
    void handleCell8(MouseEvent event) {
        cellHandler(cell8, 8);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    public static void setPictureAddress(String pictureAddress) {
        GameController.pictureAddress = pictureAddress;
    }
}
