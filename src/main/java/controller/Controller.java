package main.java.controller;

import main.java.model.Game;
import main.java.repository.Repository;
import main.java.view.GameController;
import main.java.view.View;

public class Controller {
    private View view;
    private Game game;

    public Controller() {
        view = new View(this);
        game = new Game(this);
//        playRandom(); Enable this option to play random
        view.run();
    }

    public boolean handleChange(int id) {
        int x = id % 3;
        int y = id / 3;
        game.handlePlacement(x, y);
        return true;
    }

    public View getView() {
        return view;
    }

    public Game getGame() {
        return game;
    }

    public void handleTheEnd(String statement, int state) {
        view.gameOver(statement);
        Repository.getINSTANCE().insert(game, state);
    }

    public void playRandom() {
        GameController.setSingleplayer(true);
    }
}
